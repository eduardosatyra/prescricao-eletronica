package br.com.raiadrogasil.prescricaoeletronica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class PrescricaoEletronicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(PrescricaoEletronicaApplication.class, args);
	}

}
