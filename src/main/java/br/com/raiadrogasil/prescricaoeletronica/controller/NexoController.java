package br.com.raiadrogasil.prescricaoeletronica.controller;

import br.com.raiadrogasil.prescricaoeletronica.dto.BuscaDadosNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.CpfCodNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.HistoricoPrescricaoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.QueryDefaultDTO;
import br.com.raiadrogasil.prescricaoeletronica.response.Response;
import br.com.raiadrogasil.prescricaoeletronica.service.NexoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@Api(description = "Opera��es")
public class NexoController {

    @Autowired
    private NexoService nexoService;

    @ApiOperation(value = "Buscar prescri��o eletronica por codigo e cpf.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 404, message = "Recurso n�o encontrado."),
    })
    @PostMapping("prescricaoEletronica/v1")
    public ResponseEntity<Response<BuscaDadosNexoDTO>> getPrescricaoPorCpdCodigo(@Valid @RequestBody CpfCodNexoDTO cpfCodigoValidacao, BindingResult binding) {
        Response<BuscaDadosNexoDTO> result = new Response<BuscaDadosNexoDTO>();
        if(binding.hasErrors()){
            binding.getAllErrors().forEach(error -> result.getErrors().add(error.getDefaultMessage()));
            return ResponseEntity.badRequest().body(result);
        }
        BuscaDadosNexoDTO nexo = nexoService.buscarPrescricaoPorCpfCodigo(cpfCodigoValidacao);
        result.setData(nexo);
        return ResponseEntity.ok().body(result);
    }

    @ApiOperation(value = "Buscar hitorico de prescri�oes por codigo e cpf.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Sucesso"),
            @ApiResponse(code = 400, message = "Bad Request."),
            @ApiResponse(code = 404, message = "Recurso n�o encontrado."),
    })
    @PostMapping("buscaHistoricoPrescricoes/v1")
    public ResponseEntity<Response<Collection<HistoricoPrescricaoDTO>>> getHistoricoPrescricao(@Valid @RequestBody QueryDefaultDTO q){

        Response<Collection<HistoricoPrescricaoDTO>> result = new Response<>();
        Collection<HistoricoPrescricaoDTO> listHist = nexoService.buscarHistoricoPrescricao(q);
        result.setData(listHist);

        return ResponseEntity.ok().body(result);
    }

}
