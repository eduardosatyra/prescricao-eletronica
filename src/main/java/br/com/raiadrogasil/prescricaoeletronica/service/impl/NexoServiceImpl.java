package br.com.raiadrogasil.prescricaoeletronica.service.impl;

import br.com.raiadrogasil.prescricaoeletronica.dto.BuscaDadosNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.CpfCodNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.HistoricoPrescricaoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.QueryDefaultDTO;
import br.com.raiadrogasil.prescricaoeletronica.feign.NexoFeign;
import br.com.raiadrogasil.prescricaoeletronica.service.NexoService;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.NoSuchElementException;

@Service
@Slf4j
public class NexoServiceImpl implements NexoService {


    @Value("${authorization.nexo}")
    private String auth;

    @Autowired
    private NexoFeign nexoFeign;
    Gson gson = new Gson();

    @Override
    public BuscaDadosNexoDTO buscarPrescricaoPorCpfCodigo(CpfCodNexoDTO cpfCodigoValidacao) {
        String result = nexoFeign.buscarPrescricao(auth, gson.toJson(cpfCodigoValidacao));
        BuscaDadosNexoDTO receitaNexo = gson.fromJson(result, BuscaDadosNexoDTO.class);
        return receitaNexo;
    }

    @Override
    public BuscaDadosNexoDTO buscarPrescricaoPorCpfCodigo(String cpf, String cod, String auth) {

        CpfCodNexoDTO cpfCodValidacao = CpfCodNexoDTO.builder().CPF(cpf).CodigoValidacao(cod).build();
        String result = nexoFeign.buscarPrescricaoComHeader(auth, gson.toJson(cpfCodValidacao));
        BuscaDadosNexoDTO receitaNexo = gson.fromJson(result, BuscaDadosNexoDTO.class);
        return receitaNexo;
    }

    @Override
    public Collection<HistoricoPrescricaoDTO> buscarHistoricoPrescricao(QueryDefaultDTO q) {

        String result = nexoFeign.buscarHistoricoPrescricao(auth, gson.toJson(q));
        Type collectionType = new TypeToken<Collection<HistoricoPrescricaoDTO>>(){}.getType();
        Collection<HistoricoPrescricaoDTO> listHist = gson.fromJson(result, collectionType);

        return listHist;
    }


}
