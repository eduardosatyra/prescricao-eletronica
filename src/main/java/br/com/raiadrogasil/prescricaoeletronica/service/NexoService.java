package br.com.raiadrogasil.prescricaoeletronica.service;

import br.com.raiadrogasil.prescricaoeletronica.dto.BuscaDadosNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.CpfCodNexoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.HistoricoPrescricaoDTO;
import br.com.raiadrogasil.prescricaoeletronica.dto.QueryDefaultDTO;

import java.util.Collection;
import java.util.List;

public interface NexoService {

    BuscaDadosNexoDTO buscarPrescricaoPorCpfCodigo(CpfCodNexoDTO cpfCodigoValidacao);

    BuscaDadosNexoDTO buscarPrescricaoPorCpfCodigo(String cod, String cpf, String auth);

    Collection<HistoricoPrescricaoDTO> buscarHistoricoPrescricao(QueryDefaultDTO q);

}
