package br.com.raiadrogasil.prescricaoeletronica.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Response<T> {

    private T data;
    private List<String> errors = new ArrayList<>();
}
