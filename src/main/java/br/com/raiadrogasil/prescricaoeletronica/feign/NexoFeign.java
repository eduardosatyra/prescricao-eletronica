package br.com.raiadrogasil.prescricaoeletronica.feign;

import feign.FeignException;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;

@FeignClient(url = "${url.base.nexo.dispensacao}", name = "Nexo")
public interface NexoFeign {

    @PostMapping(value = "/buscaprescricaoporcodigo", produces = "application/json", consumes = "application/json")
    String buscarPrescricao(@RequestHeader("Authorization") String auth, @RequestBody String json) throws FeignException;

    @PostMapping(value = "/buscaprescricaoporcodigo", produces = "application/json", consumes = "application/json")
    String buscarPrescricaoComHeader(@RequestHeader("Authorization") String auth, @RequestBody String json) throws FeignException;

    @PostMapping(value = "/buscahistoricoprescricoes", produces = "application/json", consumes = "application/json")
    String buscarHistoricoPrescricao(@RequestHeader("Authorization") String auth, @RequestBody String json) throws FeignException;
}

