package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class HistoricoPrescricaoDTO {

    private Long idPrescricao;
    private Date dataPrescricao;
    private String nomeMedico;
    private String telefone;

}
