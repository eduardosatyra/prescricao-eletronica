package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PacienteNexoDTO {

    private String Nascimento;
    private String idPaciente;
    private String[] Alergias;
    private String Idade;
    private String ReferenciaExterna;
    private String Documento;
    private String[] Emails;
    private String Nome;
    private ResponsaveisNexoDTO[] Responsaveis;
    private String Sexo;
    private EnderecoNexoDTO Endereco;
    private String[] Telefones;
}
