package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrescricaoNexoDTO {

    private EstabelecimentoNexoDTO Estabelecimento;
    private String idPrescricaoStatus;
    private ItensNexoDTO[] Itens;
    private String DataCriacao;
    private String[] Formulas;
    private String idPrescricao;
    private String PrescricaoAssinada;
    private String TemBeneficio;
    private String[] ItensManuais;
    private OrigemNexoDTO Origem;
    private PacienteNexoDTO Paciente;
    private String[] PlanosDesconto;
    private String ReferenciaInterna;
    private String URLQRCode;
    private String RequerReceituarioEspecial;
    private String CodigoValidacao;
    private PrescritorNexoDTO Prescritor;
    private String Comentario;
    private String[] Tags;
    private String URLBarCode;
    private String PDFUrl;
    
}
