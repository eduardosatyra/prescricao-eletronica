package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PrescritorNexoDTO {

    private String Documento;
    private String Nome;
    private float idPrescritor;
    private String Sexo;
    private String UFConselho;
    private String Numero;
    private String Conselho;
    private String UF;
}
