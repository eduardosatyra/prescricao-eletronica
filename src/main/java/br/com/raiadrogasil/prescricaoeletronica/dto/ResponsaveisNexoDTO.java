package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResponsaveisNexoDTO {

    private String GrauParentesco;
    private String Documento;
    private String Nome;
}
