package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ComplementoNexoDTO {

    private String site;
    private String telefone;
    private String Unidade;
    private String mensagem;
    private String logo;
    private String email;
    
}
