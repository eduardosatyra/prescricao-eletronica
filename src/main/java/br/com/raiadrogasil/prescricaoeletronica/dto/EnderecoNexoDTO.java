package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class EnderecoNexoDTO {

    private String Endereco1;
    private String Endereco2;
    private String Cidade;
    private String Estado;
    private String CodigoPostal;
}
