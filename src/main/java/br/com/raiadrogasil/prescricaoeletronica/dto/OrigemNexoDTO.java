package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrigemNexoDTO {

    private String Nome;
    private float idParceiro;
    private float idProntuario;

}
