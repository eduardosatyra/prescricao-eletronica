package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class CpfCodNexoDTO {

    @NotEmpty(message = "O campo CPF n�o pode ser vazio.")
    String CPF;

    @NotEmpty(message = "O campo codigoValidacao nao pode ser vazio")
    String CodigoValidacao;
}
