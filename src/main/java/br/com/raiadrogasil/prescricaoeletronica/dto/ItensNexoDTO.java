package br.com.raiadrogasil.prescricaoeletronica.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ItensNexoDTO {

    private String Posologia;
    private String ImpressaoDuasVias;
    private String PermitirTroca;
    private String AntimicrobianoDescricao;
    private String Desconto;
    private String Portaria344;
    private String AtivoAnvisa;
    private String[] ListaEANS;
    private String FormaFarmaceuticaComAcentuacao;
    private String Label;
    private String TemBeneficio;
    private String Quantidade;
    private String FormaFarmaceutica;
    private String CodigoATC;
    private String Unidade;
    private String Dispensado;
    private String idPrescricaoMedicamento;
    private String TipoMedicamentoComAcentuacao;
    private String[] Farmacos;
    private String Nome;
    private String FarmaciaPopular;
    private String UsoInterno;
    private String IdMedicamento;
    private String Antimicrobiano;
    private String UsoContinuo;
    private String TipoMedicamento;
    private String ControleEspecial;
    private String LaboratorioPreferencia;
    private String Concentracao;
    private String QuantidadeDispensada;
    private String Bula;
    private String ViaAdministracao;
    private PrincipiosAtivosNexoDTO[] PrincipiosAtivos;
}
